import Vue from "vue";
import Vuetify from "vuetify";
import router from "vue-router";

import { shallowMount, createLocalVue } from "@vue/test-utils";
import App from "@/App.vue";
const localVue = createLocalVue();

localVue.prototype.$URL = window.URL;
localVue.config.productionTip = false;
localVue.use(router);
const $pouch = {
  connect: jest.fn().mockImplementation((user, pass, db) => {
    return Promise.resolve({ smth: true });
  }),
  sync: jest.fn().mockImplementation((db, db2) => {
    return Promise.resolve({ smthelse: true });
  })
};
Vue.use(Vuetify);
describe("App.vue", () => {
  let wrapper;
  let vuetify;
  vuetify = new Vuetify();
  wrapper = shallowMount(App, {
    localVue,
    vuetify,
    mocks: {
      $pouch
    }
  });
  beforeEach(() => {});
  // it("connects to remote db", () => {
  //   expect(wrapper.vm.$databases[process.env.VUE_APP_REMOTE_COUCHDB]).not.toBe(
  //     undefined
  //   );
  // });
  // Trying to test value that gets set after the $pouch.connect
  it("the connect promise resolves", done => {
    setTimeout(() => {
      expect(wrapper.vm.$data.connected).not.toBe(undefined);
      done();
    }, 1000);
  });
  // it("connects to local db", () => {
  //   expect(wrapper.vm.$databases["productos"]).not.toBe(undefined);
  // });
});
