import Vue from "vue";
import Vuetify from "vuetify";
import VueRouter from "vue-router";
import fs from "fs";

import { mount, createLocalVue } from "@vue/test-utils";
import RevProdDesign from "@/components/RevProdDesign.vue";
const localVue = createLocalVue();
localVue.use(VueRouter);
Vue.use(Vuetify);
global.window = Object.create(window);
global.window.URL.createObjectURL = jest.fn().mockImplementation(() => {});
Vue.prototype.$URL = window.URL;
const revProd = "2-c84dc31f769147eb9954ee481ddd5bc8";
const revProdDoc = {
  _id: "01507ddf-f79c-448d-a15c-1e4974799969",
  _rev: "2-c84dc31f769147eb9954ee481ddd5bc8",
  nombre: "Conjunto",
  precio: "23424",
  descripcion: "efsfsdgdf 554",
  _attachments: {
    "pants.png": {
      content_type: "image/png",
      revpos: 2,
      digest: "md5-DjoC5E7sAGe+mITtvNLLlA==",
      length: 1674051,
      stub: true
    }
  }
};
const $pouch = {
  get: jest.fn().mockImplementation((id, config, db) => {
    if (db === "productos") return Promise.resolve(revProdDoc);
  })
};
describe("RevProdDesign.vue", () => {
  let wrapper;
  let vuetify;

  const prodWinner = {
    _id: "01507ddf-f79c-448d-a15c-1e4974799969",
    _rev: "2-c84dc31f769147eb9954ee481ddd5bc8",
    nombre: "Conjunto",
    precio: "23424",
    descripcion: "efsfsdgdf 554",
    _conflicts: ["2-92ed8ddf492a45098e046f3c565d76aa"],
    _attachments: {
      "pants.png": {
        content_type: "image/png",
        revpos: 2,
        digest: "md5-DjoC5E7sAGe+mITtvNLLlA==",
        length: 1674051,
        stub: true
      }
    }
  };
  vuetify = new Vuetify();
  wrapper = mount(RevProdDesign, {
    localVue,
    vuetify,
    propsData: { revProd, prodWinner },
    mocks: {
      $pouch
    }
  });
  it("the name is displayed", () => {
    expect(wrapper.text().toLowerCase()).toContain(
      revProdDoc.nombre.toLowerCase()
    );
  });
  it("the price is displayed", () => {
    expect(wrapper.text().toLowerCase()).toContain(
      revProdDoc.precio.toLowerCase()
    );
  });
  it("the accepted revison removes all conflicts", () => {
    wrapper.vm.Aceptado();
    expect(wrapper.vm.$data.show).toBe(false);
    expect(wrapper.emitted()["rev-checked"]).toBeTruthy();
    expect(wrapper.vm.$data.dialog).toBe(false);
  });
});
