import Vue from "vue";
import Vuetify from "vuetify";
import VueRouter from "vue-router";

import { shallowMount, createLocalVue } from "@vue/test-utils";
import Layout from "@/components/Layout.vue";
const localVue = createLocalVue();
//localVue.use(VueRouter);
Vue.use(Vuetify);
const $pouch = {
  connect: jest.fn().mockImplementation((user, password, db) => {
    return Promise.resolve({ connected: true });
  }),
  disconnect: jest.fn().mockImplementation(db => {
    return Promise.resolve({ disconnected: true });
  })
};
describe("CheckRevision.vue", () => {
  let wrapper;
  let vuetify;
  vuetify = new Vuetify();
  wrapper = shallowMount(Layout, {
    localVue,
    vuetify,
    mocks: {
      $route: {
        name: "default"
      },
      $pouch
    }
  });
  it("the database is synced by default", () => {
    expect(wrapper.vm.$data.switch1).toBe(true);
  });
  it("the changeStatus function inverts the status of the sync", async () => {
    expect(wrapper.vm.$data.switch1).toBe(true);
    wrapper.vm.changeStatus();
    expect($pouch.connect).toHaveBeenCalled();
  });
});
