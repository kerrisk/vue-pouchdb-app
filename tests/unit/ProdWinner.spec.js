import Vue from "vue";
import Vuetify from "vuetify";
import VueRouter from "vue-router";

import { mount, createLocalVue } from "@vue/test-utils";
import ProdWinner from "@/components/ProdWinner.vue";
const localVue = createLocalVue();
localVue.use(VueRouter);
Vue.use(Vuetify);
global.window = Object.create(window);
global.window.URL.createObjectURL = jest.fn().mockImplementation(() => {});
Vue.prototype.$URL = window.URL;

describe("ProdWinner.vue", () => {
  let wrapper;
  let vuetify;
  const product = {
    _id: "01507ddf-f79c-448d-a15c-1e4974799969",
    _rev: "2-c84dc31f769147eb9954ee481ddd5bc8",
    nombre: "Conjunto",
    precio: "23424",
    descripcion: "efsfsdgdf 554",
    _conflicts: ["2-92ed8ddf492a45098e046f3c565d76aa"],
    _attachments: {
      "pants.png": {
        content_type: "image/png",
        revpos: 2,
        digest: "md5-DjoC5E7sAGe+mITtvNLLlA==",
        length: 1674051,
        stub: true
      }
    }
  };
  vuetify = new Vuetify();
  wrapper = mount(ProdWinner, {
    localVue,
    vuetify,
    propsData: { product }
  });
  it("the name is displayed", () => {
    expect(wrapper.text().toLowerCase()).toContain(
      product.nombre.toLowerCase()
    );
  });
  it("the price is displayed", () => {
    expect(wrapper.text().toLowerCase()).toContain(
      product.precio.toLowerCase()
    );
  });
  it("the product has no attachments", () => {
    wrapper = mount(ProdWinner, {
      localVue,
      vuetify,
      propsData: { product: { ...product, _attachments: undefined } }
    });
    expect(wrapper.vm.image).toBe(undefined);
  });
  it("the product has no _id", () => {
    wrapper = mount(ProdWinner, {
      localVue,
      vuetify,
      propsData: { product: { ...product, _id: undefined } }
    });
    expect(wrapper.vm.productId).toBe("");
  });
});
