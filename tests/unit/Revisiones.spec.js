import Vue from "vue";
import Vuetify from "vuetify";
import VueRouter from "vue-router";
import fs from "fs";

import { shallowMount, createLocalVue } from "@vue/test-utils";
import Revisiones from "@/views/Revisiones.vue";
const localVue = createLocalVue();
localVue.use(VueRouter);
Vue.use(Vuetify);
const $pouch = {
  allDocs: jest.fn().mockImplementation((config, db) => {
    if (db === "productos")
      return new Promise((resolve, reject) => {
        fs.readFile("__mock__/productos_conflic.json", (err, data) => {
          if (err != null) {
            reject(err);
          }
          console.log(data);
          resolve(JSON.parse(data));
        });
      });
  })
};
describe("Revisiones.vue", () => {
  let wrapper;
  let vuetify;
  vuetify = new Vuetify();
  wrapper = shallowMount(Revisiones, {
    localVue,
    vuetify,
    mocks: {
      $pouch
    }
  });
  it("the products are loaded from pouchdb", done => {
    expect.assertions(1);
    // TODO: test the error
    setTimeout(() => {
      expect(wrapper.vm.$data.conflictedProducts).not.toBe(undefined);
      done();
    }, 1000);
  });
  // it("the deleted function triggers the deleted product snackbar", () => {
  //   expect(wrapper.vm.$data.snackbarDeleted).toBe(false);
  //   wrapper.vm.deleted();
  //   expect(wrapper.vm.$data.snackbarDeleted).toBe(true);
  // });
  // it("the modified function triggers the modified product snackbar", () => {
  //   expect(wrapper.vm.$data.snackbarModified).toBe(false);
  //   wrapper.vm.modified();
  //   expect(wrapper.vm.$data.snackbarModified).toBe(true);
  // });
});
