import Vue from "vue";
import Vuetify from "vuetify";
import VueRouter from "vue-router";

import { mount, createLocalVue } from "@vue/test-utils";
import ProductCardDesign from "@/components/ProductCardDesign.vue";
const localVue = createLocalVue();
localVue.use(VueRouter);
Vue.use(Vuetify);
global.window = Object.create(window);
global.window.URL.createObjectURL = jest.fn().mockImplementation(() => {});
Vue.prototype.$URL = window.URL;

describe("Home.vue", () => {
  let wrapper;
  let vuetify;
  const product = {
    _id: "266d1d5d-a2d9-4a07-81a5-328d66b055c6",
    _rev: "2-ecde6e7057504189b0e7e56eb1246631",
    nombre: "prueba2",
    precio: "123",
    descripcion: "wt43twefsd ",
    _attachments: {
      "Solid_black.png": {
        content_type: "image/png",
        revpos: 2,
        digest: "md5-rnA239hwA93yf9k1p/sR3g==",
        data:
          "iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAIAAAAiOjnJAAAABGdBTUEAALGPC/xhBQAAAiJJREFUeF7t0IEAAAAAw6D5Ux/khVBhwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDDwMDDVlwABBWcSrQAAAABJRU5ErkJggg=="
      }
    }
  };
  vuetify = new Vuetify();
  wrapper = mount(ProductCardDesign, {
    localVue,
    vuetify,
    propsData: { product }
  });
  it("the name is displayed", () => {
    expect(wrapper.text().toLowerCase()).toContain(
      product.nombre.toLowerCase()
    );
  });
  it("the price is displayed", () => {
    expect(wrapper.text().toLowerCase()).toContain(
      product.precio.toLowerCase()
    );
  });
  it("the product has no attachments", () => {
    wrapper = mount(ProductCardDesign, {
      localVue,
      vuetify,
      propsData: { product: { ...product, _attachments: undefined } }
    });
    expect(wrapper.vm.image).toBe(undefined);
  });
  it("the dialog is closed", () => {
    wrapper.vm.closeDialog();
    expect(wrapper.vm.$data.dialog).toBe(false);
  });
  it("product deleted triggers emit", () => {
    wrapper.vm.ProductDeleted();
    expect(wrapper.emitted()["product-deleted"]).toBeTruthy();
  });
  it("product modified triggers emit", () => {
    wrapper.vm.ProductModified();
    expect(wrapper.emitted()["product-modified"]).toBeTruthy();
  });
  // it("the deleted function triggers the deleted product snackbar", () => {
  //   expect(wrapper.vm.$data.snackbarDeleted).toBe(false);
  //   wrapper.vm.deleted();
  //   expect(wrapper.vm.$data.snackbarDeleted).toBe(true);
  // });
  // it("the modified function triggers the modified product snackbar", () => {
  //   expect(wrapper.vm.$data.snackbarModified).toBe(false);
  //   wrapper.vm.modified();
  //   expect(wrapper.vm.$data.snackbarModified).toBe(true);
  // });
});
