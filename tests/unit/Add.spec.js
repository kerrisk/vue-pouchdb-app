import Vue from "vue";
import Vuetify from "vuetify";
import VueRouter from "vue-router";
import fs from "fs";

import { shallowMount, createLocalVue } from "@vue/test-utils";
import Add from "@/views/Add.vue";
const localVue = createLocalVue();
localVue.use(VueRouter);
Vue.use(Vuetify);
const $pouch = {
  get: jest.fn().mockImplementation((id, config, db) => {
    if (db === "productos")
      return new Promise((resolve, reject) => {
        fs.readFile("__mock__/productos_conflicting_ver.json", (err, data) => {
          if (err != null) {
            reject(err);
          }
          resolve(JSON.parse(data));
        });
      });
  }),
  remove: jest.fn().mockImplementation((doc, config, db) => {
    if (doc._id) return Promise.resolve({ deleted: true });
  }),
  put: jest.fn().mockImplementation((doc, config, db) => {
    if (doc._id) return Promise.resolve({ updated: true });
  })
};
describe("Add.vue", () => {
  let wrapper;
  let vuetify;
  vuetify = new Vuetify();
  wrapper = shallowMount(Add, {
    localVue,
    vuetify,
    mocks: {
      // $route,
      $pouch
    }
  });
  it("loads the component", () => {
    expect(wrapper.text()).not.toBe(undefined);
  });
  // it("the products are loaded from pouchdb", done => {
  //   expect.assertions(1);
  //   // TODO: test the error
  //   setTimeout(() => {
  //     expect(wrapper.vm.$data.productWinner).not.toBe(undefined);
  //     done();
  //   }, 1000);
  // });
  // it("the accepted revison removes all conflicts", () => {
  //   wrapper.vm.aceptarRevision();
  //   expect($pouch.remove).toHaveBeenCalled();
  //   expect($pouch.put).toHaveBeenCalled();
  // });
  // it("the changeWinner function updates the winner product", () => {
  //   wrapper.vm.changeWinner({ new: "otroproducto", _id: "nuevoid-123" });
  //   expect(wrapper.vm.$data.productWinner).toMatchObject({
  //     new: "otroproducto"
  //   });
  // });
});
