import { shallowMount } from "@vue/test-utils";
import HelloWorld from "@/components/HelloWorld.vue";
import vuetify from "vuetify";
import Vue from "vue";

describe("HelloWorld.vue", () => {
  beforeEach(() => {
    Vue.use(vuetify);
  });
  it("renders props.msg when passed", () => {
    const msg = "new message";
    const wrapper = shallowMount(HelloWorld, {
      propsData: { msg }
    });
    expect(wrapper.text()).toMatch(msg);
  });
});
