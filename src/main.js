import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import PouchDB from "pouchdb-browser";
import PouchFind from "pouchdb-find";
import PouchLiveFind from "pouchdb-live-find";
import PouchAuthentification from "pouchdb-authentication";
import PouchVue from "pouch-vue";
import pouchdbDebug from "pouchdb-debug";

PouchDB.plugin(PouchAuthentification);
PouchDB.plugin(pouchdbDebug);
PouchDB.plugin(PouchFind);
PouchDB.plugin(PouchLiveFind);
PouchDB.debug.disable();

Vue.use(PouchVue, {
  pouch: PouchDB, // optional if `PouchDB` is available on the global object
  defaultDB: "todos", // this is used as a default connect/disconnect database
  optionDB: {}, // this is used to include a custom fetch() method (see TypeScript example)
  debug: "*" // optional - See `https://pouchdb.com/api.html#debug_mode` for valid settings (will be a separate Plugin in PouchDB 7.0)
});
Vue.prototype.$URL = window.URL;
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
